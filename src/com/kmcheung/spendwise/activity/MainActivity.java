package com.kmcheung.spendwise.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import com.kmcheung.spendwise.R;
import com.kmcheung.spendwise.Util;
import com.kmcheung.spendwise.activity.chart.PersonalChartActivity;
import com.kmcheung.spendwise.schema.PersonalExpenseSchema;
import com.parse.*;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    public static final String tag = MainActivity.class.getName();
    private String objId;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setupParse();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setupButtons();
    }

    private void setupButtons() {
        try {
            findViewById(R.id.btn_anon_login).setOnClickListener(this);
            findViewById(R.id.btn_signup).setOnClickListener(this);
            findViewById(R.id.btn_login).setOnClickListener(this);
            findViewById(R.id.btn_logout).setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupParse() {
//        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "KPNlcIDghuJt1HmtUbQ8PutrWNcvKqD8tMu2gt7H", "8KqqEz4UqzoDChEtV662aFTlbpLOC7Uquk1Kcdgy");
        ParseUser.enableAutomaticUser();
        anonymousLogin();
    }

    private void anonymousLogin() {
        ParseUser.getCurrentUser().increment("RunCount");
        ParseUser.getCurrentUser().saveInBackground();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                String username = ((EditText) findViewById(R.id.et_username)).getText().toString();
                String password = ((EditText) findViewById(R.id.et_password)).getText().toString();
                login(username, password);
                break;
            case R.id.btn_signup:
                username = ((EditText) findViewById(R.id.et_username)).getText().toString();
                password = ((EditText) findViewById(R.id.et_password)).getText().toString();
                signUp(username, password);
                break;
            case R.id.btn_logout:
                logout();
                break;
            case R.id.btn_anon_login:
                startPersonalExpenseActivity();
                break;
            default:
                break;
        }
    }

    private void signUp(final String username, String password) {
        ParseUser.getCurrentUser().logOut();
        ParseQuery<ParseUser> query = ParseUser.getQuery().whereEqualTo("username", username);
        try {
            List<ParseUser> result = query.find();
            if (result.size() > 0) {
                Util.makeToast(this, "Username " + username + " already in use");
            } else {
                ParseUser user = ParseUser.getCurrentUser();
                user.setUsername(username);
                user.setPassword(password);
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Util.makeToast(MainActivity.this, "Hello " + username + ". Welcome to SpendWise ");
                            Log.d(tag, "successfully signup");
                            startPersonalExpenseActivity();
                        } else {
                            Util.makeToast(MainActivity.this, e.getMessage());
                            Log.d(tag, e.getMessage());
                        }
                    }
                });
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void login(String username, String password) {
        try {
            ParseUser user = ParseUser.getCurrentUser();
            LogInCallback logInCallback = new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    if (e == null) {
                        Util.makeToast(MainActivity.this, "Login success");
                        startPersonalExpenseActivity();
                    } else {
                        Util.makeToast(MainActivity.this, "Username Password mismatch");
                        Log.d(tag, e.getMessage());
                    }
                }
            };
            if (user == null || user.getUsername() == null) { // after logout
                ParseUser.logInInBackground(username, password, logInCallback);
            } else if (!user.getUsername().equals(username)) { //in case of anonymous user
                user.logInInBackground(username, password, logInCallback);
            } else { //already logged in
                Util.makeToast(this, "Welcome back " + username);
                startPersonalExpenseActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startPersonalExpenseActivity() {
        Intent intent = new Intent(this, PersonalExpenseActivity.class);
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logout() {
        ParseUser user = ParseUser.getCurrentUser();
        user.logOut();
        anonymousLogin();
        Util.makeToast(this, "Logged out");
    }

}
