package com.kmcheung.spendwise.activity.chart;

import android.content.Context;
import com.kmcheung.spendwise.Util;
import com.kmcheung.spendwise.schema.PersonalExpenseSchema;
import com.parse.ParseObject;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alancheung on 3/12/14.
 */
public class PieChartViewFactory {
    public static HashMap<String, Double> processData(List<ParseObject> data, List<String> filterMask) {
        HashMap<String, Double> processedData = new HashMap<String, Double>(filterMask.size());

        if (filterMask.size()==0) {
            return processedData;
        }


        for (String type: filterMask) {
            processedData.put(type, new Double(0));
        }

        for (ParseObject item: data) {
            String cat = item.getString(PersonalExpenseSchema.category);
            if (filterMask.contains(cat)) {
                Double amount = item.getDouble(PersonalExpenseSchema.amount);
                Double cumulativeAmount = processedData.get(cat);
                processedData.put(cat, amount + cumulativeAmount);
            }
        }
        return processedData;
    }

    public static GraphicalView getPieChartView(Context context, List<ParseObject> personalExpenses, List<String> mask) {
        List<String> filterMask = new ArrayList<String>(mask);
        if (filterMask.contains("Gross Spending")) {
            filterMask.remove("Gross Spending");
        }
        int kind = filterMask.size();
        int[] colors = new int[kind];
        for (int i = 0; i< kind; i ++) {
            colors[i] = Util.nextColor();
        }
        DefaultRenderer renderer = buildCategoryRenderer(colors);
        renderer = configureRenderer(renderer);
        HashMap<String, Double> processedData = processData(personalExpenses, filterMask);
        double[] values = new double[processedData.size()];
        String[] titles = new String[processedData.size()];
        int i = 0;

        for (Map.Entry<String, Double> entry : processedData.entrySet()) {
            titles[i] = entry.getKey();
            values[i] = entry.getValue();
            i ++;

        }

        CategorySeries series = buildCategoryDataset("", values, titles);


        return ChartFactory.getPieChartView(context,series, renderer);
    }

    private static DefaultRenderer configureRenderer(DefaultRenderer renderer) {
        renderer.setZoomButtonsVisible(true);
        renderer.setZoomEnabled(true);
        renderer.setChartTitleTextSize(20);
        renderer.setDisplayValues(true);
        renderer.setShowLabels(false);
        return renderer;
    }


    protected static DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[] { 20, 30, 15, 0 });
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    protected static CategorySeries buildCategoryDataset(String title, double[] values, String[] catagories) {
        CategorySeries series = new CategorySeries(title);
        if (values.length != catagories.length) throw new IllegalArgumentException("Argument length mismatch");
        for (int i = 0; i < values.length; i++) {
            series.add(catagories[i], values[i]);
        }

        return series;
    }
}
