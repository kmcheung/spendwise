package com.kmcheung.spendwise.activity.chart;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.kmcheung.spendwise.R;
import com.kmcheung.spendwise.Util;
import com.kmcheung.spendwise.schema.PersonalExpenseSchema;
import com.kmcheung.spendwise.view.MultiSpinner;
import com.parse.*;
import org.achartengine.GraphicalView;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by alancheung on 30/11/14.
 */
public class PersonalChartActivity extends Activity implements AdapterView.OnItemSelectedListener {

    private String tag = PersonalChartActivity.class.getName();
    private GraphicalView mChartView;
    private List<ParseObject> personalExpenses = new ArrayList<ParseObject>();
    //    private boolean[] categoryFilter = new boolean[]{};
    private String[] categories;
    private List<String> filterMask = new ArrayList<String>();
    private AtomicBoolean isDataReady = new AtomicBoolean();
    private int summaryPeriod = 14;
    private enum RenderType {Line, Pie, UNDEFINED};
    private RenderType chartToRender;
    private Date lastQueryDate = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_charts);
        categories = getResources().getStringArray(R.array.filter_category);
        filterMask.add(0, categories[0]);
        LineChartViewFactory.categories = categories;
        setupSpinner();
    }

    private void setupSpinner() {
        MultiSpinner ms = (MultiSpinner) findViewById(R.id.sp_filter);

        ms.setItems(Arrays.asList(getResources().getStringArray(R.array.filter_category)), "All", new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
//                categoryFilter = selected;
                int size = selected.length;

                Set<String> selectedItems = new HashSet<String>();
                for (int i = 0; i < size; i++) {
                    if (selected[i]) {
                        selectedItems.add(categories[i]);
                    }
                }
                if (!(filterMask.containsAll(selectedItems) && filterMask.size() == selectedItems.size())) {
                    filterMask.clear();
                    filterMask.addAll(selectedItems);
                    renderChart();
                }
            }
        });

        ArrayAdapter<String> periodAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.selection_period));
        ArrayAdapter<String> chartTypeAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, getResources().getStringArray(R.array.personal_summary_list));

        Spinner chartType = ((Spinner) findViewById(R.id.sp_chart_type));
        chartType.setOnItemSelectedListener(this);
        chartType.setAdapter(chartTypeAdapter);
        Spinner period = ((Spinner) findViewById(R.id.sp_period));
        period.setOnItemSelectedListener(this);
        period.setAdapter(periodAdapter);
    }


    private void queryData(Date afterDate) {
        isDataReady.set(false);
        ParseQuery<ParseObject> query = ParseQuery.getQuery(PersonalExpenseSchema.className);
//        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        query.orderByAscending(PersonalExpenseSchema.date);
        query.whereEqualTo(PersonalExpenseSchema.memberName, ParseUser.getCurrentUser().getUsername());
        if (afterDate != null) {
            query.whereGreaterThan(PersonalExpenseSchema.date, afterDate);
        }
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    isDataReady.set(true);
                    personalExpenses = list;
                }
                renderChart();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_chart_type:
                chartTypeSelected(view, position, id);
                break;
            case R.id.sp_period:
                displayPeriodSelected(view, position, id);
                break;
            default:
                break;
        }
    }

    private void displayPeriodSelected(View view, int position, long id) {
        Date date;
        switch (position) {
            case 0:
                date = Util.timeBeforeNow(0, 0, 14);
                summaryPeriod = 14;
                break;
            case 1:
                date = Util.timeBeforeNow(0, 1, 0);
                summaryPeriod = 30;
                break;
            case 2:
                date = Util.timeBeforeNow(0, 3, 0);
                summaryPeriod = 91;
                break;
            case 3:
                date = Util.timeBeforeNow(0, 6, 0);
                summaryPeriod = 182;
                break;
            case 4:
                date = Util.timeBeforeNow(1, 0, 0);
                summaryPeriod = 365;
                break;
            default:
                date = null;
                break;
        }
        if (!Util.isSameDate(date, lastQueryDate)) {
            lastQueryDate = date;
            queryData(date);
        }
    }

    private void chartTypeSelected(View view, int position, long id) {
        switch (position) {
            case 0:
                chartToRender = RenderType.Line;
                renderChart();
                break;
            case 1:
                chartToRender = RenderType.Pie;
                renderChart();
                break;
            case 2:
            case 3:
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void renderChart() {
        switch (chartToRender) {
            case Line:
                renderLineChart();
                break;
            case Pie:
                renderPieChart();
                break;
            default:
                break;
        }
    }

    private void renderPieChart() {

        if (!isDataReady.get()) return;
        if (filterMask.contains(categories[0]) && filterMask.size() == 1) {
            Util.makeToast(this, "Please specify category to display");
            return;
        }
        if (personalExpenses.size() == 0) {
            Util.makeToast(this, "No data found on your acc.");
            return;
        }
        try {
            LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
            layout.removeAllViews();

            mChartView = PieChartViewFactory.getPieChartView(this, personalExpenses, filterMask);

            layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT));
            mChartView.zoomReset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void renderLineChart() {
        if (!isDataReady.get()) return;
        if (personalExpenses.size() == 0) {
            Util.makeToast(this, "No data found on your acc.");
            return;
        }
        try {
            LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
            layout.removeAllViews();

            int binSize = summaryPeriod/30;
            binSize = binSize <= 0? 1:binSize;
            mChartView = LineChartViewFactory.getTimeChartView(this, personalExpenses, filterMask, binSize);

            layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT));
            mChartView.zoomReset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
