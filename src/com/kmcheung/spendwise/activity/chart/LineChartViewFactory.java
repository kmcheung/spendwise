package com.kmcheung.spendwise.activity.chart;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import com.kmcheung.spendwise.Util;
import com.kmcheung.spendwise.schema.PersonalExpenseSchema;
import com.parse.ParseObject;
import org.achartengine.*;
import org.achartengine.model.*;
import org.achartengine.renderer.*;

import java.util.*;

/**
 * Created by alancheung on 1/12/14.
 */
public class LineChartViewFactory {
    private static String[] xAxisResolution = {"dd/MM", "MM/yy"};
    private static String xAxisMin = "";
    private static String xAxisMax = "";
    private static String filterNone = "Gross Spending";

    public static String[] categories;
    //Convert rows of expense data into a nested category-> (timestaml-> spending) mapping
    private static HashMap<String, HashMap<String, Double>> flatten(List<ParseObject> personalExpenses, List<String> filterMask, int binSize) {
        HashMap<String, HashMap<String, Double>> expenses = new HashMap<String, HashMap<String, Double>>();
        int filterCount = filterMask == null? 0: filterMask.size();
        for (String cat: categories) {
            expenses.put(cat, new HashMap<String, Double>());
        }

        Date baseDate = personalExpenses.get(0).getDate(PersonalExpenseSchema.date);
        for (ParseObject item : personalExpenses) {
            Date date = item.getDate(PersonalExpenseSchema.date);

            String timestamp = Util.getTimeStamp(baseDate, date, binSize);
            String cat = item.getString(PersonalExpenseSchema.category);
            Double amount = item.getDouble(PersonalExpenseSchema.amount);

            if (filterCount == 0) {
                HashMap<String, Double> innerMap = expenses.get(filterNone);
                if (innerMap.containsKey(timestamp)) {
                    innerMap.put(timestamp, innerMap.get(timestamp) + amount);
                } else {
                    innerMap.put(timestamp, amount);
                }
            } else {

                for (String filter: filterMask) {
                    HashMap<String, Double> innerMap = expenses.get(filter);
                    innerMap.put(timestamp, 0d);
                }

                if (filterMask.contains(cat)) {
                    HashMap<String, Double> innerMap = expenses.get(cat);
                    if (innerMap.containsKey(timestamp)) {
                        innerMap.put(timestamp, innerMap.get(timestamp) + amount);
                    } else {
                        innerMap.put(timestamp, amount);
                    }
                }
                if (filterMask.contains(filterNone)) {
                    HashMap<String, Double> innerMap = expenses.get(filterNone);
                    if (innerMap.containsKey(timestamp)) {
                        innerMap.put(timestamp, innerMap.get(timestamp) + amount);
                    } else {
                        innerMap.put(timestamp, amount);
                    }
                }
            }
        }
        return expenses;
    }

    private static XYSeriesRenderer newSeriesRenderer() {
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setPointStyle(Util.nextPointStyle());
        renderer.setColor(Util.nextColor());
        renderer.setFillPoints(true);
        renderer.setDisplayChartValues(true);
        renderer.setDisplayChartValuesDistance(10);
        return renderer;
    }

    private static XYMultipleSeriesRenderer configMultipleRenderer(double xMin, double xMax) {
        XYMultipleSeriesRenderer seriesRenderer = new XYMultipleSeriesRenderer();

        seriesRenderer.setApplyBackgroundColor(true);
        seriesRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
        seriesRenderer.setXAxisMin(xMin);
        seriesRenderer.setXAxisMin(xMax);
        seriesRenderer.setAxisTitleTextSize(16);
        seriesRenderer.setChartTitleTextSize(20);
        seriesRenderer.setLabelsTextSize(15);
        seriesRenderer.setLegendTextSize(15);
        seriesRenderer.setMargins(new int[]{20, 30, 15, 0});
        seriesRenderer.setZoomButtonsVisible(true);
        seriesRenderer.setPointSize(5);
        return seriesRenderer;
    }

    private static List<TimeSeries> processDataPoints(List<ParseObject> data, List<String> filterMask, int binSize) {
        Date baseDate = data.get(0).getDate(PersonalExpenseSchema.date);
        int seriesLength = (filterMask == null) ? 0: filterMask.size();
        TimeSeries[] allSeries = new TimeSeries[seriesLength == 0?1:seriesLength];
        if (seriesLength == 0) {
            allSeries[0] = new TimeSeries(filterNone);
        } else {
            for (int i = 0; i < seriesLength; i++) {
                allSeries[i] = new TimeSeries(filterMask.get(i));
            }
        }


        HashMap<String, List<String>> ascendingDates = new HashMap<String, List<String>>();
        if (seriesLength == 0) {
            List<String> arr = new ArrayList<String>();
            ascendingDates.put(filterNone, arr);
        } else {
            for (int i = 0; i < seriesLength; i++) {
                List<String> arr = new ArrayList<String>();
                ascendingDates.put(filterMask.get(i), arr);
            }
        }
        HashMap<String,HashMap<String, Double>> map = flatten(data, filterMask, binSize);

        for (ParseObject entry : data) {
            Date date = entry.getDate(PersonalExpenseSchema.date);
//            String category = entry.getString(PersonalExpenseSchema.category);
            String timestamp = Util.getTimeStamp(baseDate, date, binSize);
            if (seriesLength == 0) {
                List<String> ascDate = ascendingDates.get(filterNone);
                if (!ascDate.contains(timestamp)) {
                    Double value = map.get(filterNone).get(timestamp);
                    allSeries[0].add(date, value);
                    ascDate.add(timestamp);
                }
            } else {
                for (String category: filterMask) {
                    List<String> ascDate = ascendingDates.get(category);
                    if (!ascDate.contains(timestamp)) {
                        Double value = map.get(category).get(timestamp);
                        int index = filterMask.indexOf(category);
                        allSeries[index].add(date, value);
                        ascDate.add(timestamp);
                    }
                }
//                if (filterMask.contains(filterNone)) {
//                    List<String> ascDate = ascendingDates.get(filterNone);
//                    if (!ascDate.contains(timestamp)) {
//                        Double value = map.get(filterNone).get(timestamp);
//                        int index = filterMask.indexOf(filterNone);
//                        allSeries[index].add(date, value);
//                        ascDate.add(timestamp);
//                    }
//                }
            }
        }
//        xAxisMin = ascendingDates.get(0);
//        xAxisMax = ascendingDates.get(ascendingDates.size()-1);
        return Arrays.asList(allSeries);
    }

    public static GraphicalView getTimeChartView(final Context context, List<ParseObject> data, List<String> filterMask, int binSize) {

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        List<TimeSeries> allSeries = processDataPoints(data, filterMask, binSize);
        for (TimeSeries ts: allSeries) {
            dataset.addSeries(ts);
        }

        double xMin = data.get(0).getDate(PersonalExpenseSchema.date).getTime();
        double xMax = data.get(data.size()-1).getDate(PersonalExpenseSchema.date).getTime();
        XYMultipleSeriesRenderer currentRenderer = configMultipleRenderer(xMin, xMax);
        int numOfRenderer = filterMask.size() > 0? filterMask.size(): 1;
        for (int i =0; i<numOfRenderer; i++) {
            currentRenderer.addSeriesRenderer(newSeriesRenderer());
        }
        currentRenderer.setClickEnabled(true);
        currentRenderer.setSelectableBuffer(10);

        final GraphicalView chartView = ChartFactory.getTimeChartView(context, dataset, currentRenderer, xAxisResolution[0]);
        // enable the chart click events
        chartView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // handle the click event on the chart
                SeriesSelection seriesSelection = chartView.getCurrentSeriesAndPoint();
                if (seriesSelection == null) {
                    Util.makeToast(context, "No chart element");
                } else {
                    // display information of the clicked point
                    Util.makeToast(
                            context,
                            "Chart element in series index " + seriesSelection.getSeriesIndex()
                                    + " data point index " + seriesSelection.getPointIndex() + " was clicked"
                                    + " closest point value X=" + seriesSelection.getXValue() + ", Y="
                                    + seriesSelection.getValue());
                }
            }
        });
        return chartView;
    }
}
