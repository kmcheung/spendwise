package com.kmcheung.spendwise.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.kmcheung.spendwise.R;
import com.kmcheung.spendwise.Util;
import com.kmcheung.spendwise.activity.chart.PersonalChartActivity;
import com.kmcheung.spendwise.schema.ExpenseCategory;
import com.kmcheung.spendwise.schema.PersonalExpense;
import com.kmcheung.spendwise.schema.PersonalExpenseSchema;
import com.kmcheung.spendwise.view.TextValidator;
import com.parse.*;

import java.text.*;
import java.util.*;

/**
 * Created by alancheung on 24/11/14.
 */
public class PersonalExpenseActivity extends Activity implements View.OnClickListener {
    public static List<String> categories = new ArrayList<String>(Arrays.asList(
            ExpenseCategory.Transport,
            ExpenseCategory.Food,
            ExpenseCategory.Entertainment,
            ExpenseCategory.Clothing,
            ExpenseCategory.Groceries,
            ExpenseCategory.Accomodation,
            ExpenseCategory.Travel,
            ExpenseCategory.Telecommunication,
            ExpenseCategory.Others
    ));
    private Spinner spinner = null;
    private ProgressBar loadingSpinner;
    private final String tag = PersonalExpenseActivity.class.getName();
    private int daysOfData = 0;
    private TextView tvSeekBarStatus;
    private String daysOfDataDescription;
    private SeekBar sb;
    private EditText etAmount;
    private EditText etDate;
    private EditText etDescription;
    private Button demoButton;

    private void setupButtons() {
        try {
            findViewById(R.id.btn_summary).setOnClickListener(this);
            findViewById(R.id.btn_add_expense).setOnClickListener(this);
            demoButton = (Button)findViewById(R.id.btn_generate_demo_data);
            demoButton.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupDropDownMenu() {
        try {
            spinner = (Spinner) findViewById(R.id.sp_category);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item, categories);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.personal_expense);
            setupEditTexts();
            setupButtons();
            setupSeekBar();
            setupDropDownMenu();
            setupLoadingSpinner();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupLoadingSpinner() {
        loadingSpinner = (ProgressBar)findViewById(R.id.loading_spinner);
        loadingSpinner.setVisibility(View.INVISIBLE);
    }

    private void setupEditTexts() {
        try {
            etAmount = (EditText)findViewById(R.id.et_amount);
            etDate = (EditText)findViewById(R.id.et_date);
            etDescription = (EditText)findViewById(R.id.et_description);
            etAmount.addTextChangedListener(new TextValidator(etAmount) {
                @Override
                public void validate(TextView textView, String text) {
                    try {
                        Double.parseDouble(text);
                    } catch (NumberFormatException exception) {
                        textView.setError("Number only");
                    }
                }
            });

            etDate.addTextChangedListener(new TextValidator(etDate) {
                @Override
                public void validate(TextView textView, String text) {
                    try {
                        Util.getDateFromDDMMYYYY(text);
                    } catch (java.text.ParseException e) {
                        textView.setError("DDMMYYYY");
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupSeekBar() {
        sb = (SeekBar)findViewById(R.id.sb_demo_data);
        daysOfDataDescription = getString(R.string.days_of_data);
        tvSeekBarStatus = (TextView) findViewById(R.id.tv_days_of_demo);
        tvSeekBarStatus.setText(sb.getProgress() + " " + daysOfDataDescription);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvSeekBarStatus.setText(progress + " " + daysOfDataDescription);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
// Nothing to do
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
// Nothing to do
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_expense:
                addExpense();
                break;
            case R.id.btn_summary:
                showSummary();
                break;
            case R.id.btn_generate_demo_data:
                daysOfData = sb.getProgress();
                generateDemoData(daysOfData);
                break;
            default:
                break;
        }
    }

    private void addExpense() {
        try {
            Double amout = Double.parseDouble(etAmount.getText().toString());
            Date date = Util.getDateFromDDMMYYYY(etDate.getText().toString());
            String des = etDescription.getText().toString();
            String cat = spinner.getSelectedItem().toString();
            PersonalExpense.Builder builder = new PersonalExpense.Builder()
                    .setAmount(amout)
                    .setDate(date)
                    .setDescription(des)
                    .setCategory(cat);
            PersonalExpense.save(this, builder.build());
        } catch (Exception e) {
            Util.makeToast(this, e.getMessage());
        }
    }

    private void generateDemoData(int daysOfData) {

        try {
            loadingSpinner.setVisibility(View.VISIBLE);
            demoButton.setClickable(false);
            GenerateDataTask genTask = new GenerateDataTask();
            genTask.execute(new Integer[]{daysOfData});
        } catch (Exception e) {
            Util.makeToast(this, e.getMessage());
            e.printStackTrace();
        }
    }

    private void showSummary() {
        Intent intent = new Intent(this, PersonalChartActivity.class);
        startActivity(intent);
    }

    private class GenerateDataTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            int daysOfData = params[0];
            ParseUser curUser = ParseUser.getCurrentUser();
            ParseACL testACL = null;
            if (curUser.getObjectId() != null) {
                testACL = new ParseACL(ParseUser.getCurrentUser());
                testACL.setPublicReadAccess(true);
                testACL.setPublicWriteAccess(true);
            }
            List<PersonalExpense> expenses = new ArrayList<PersonalExpense>();
            int day = 0;
            int size = categories.size();
            Random r = new Random();

            for (int i = 0; i < daysOfData; i++) {
                PersonalExpense.Builder builder = new PersonalExpense.Builder();
                ParseObject test = new ParseObject(PersonalExpenseSchema.className);
                if (testACL != null) {
                    test.setACL(testACL);
                }

                builder.setDescription("Testing Objects")
                        .setDate(Util.timeBeforeNow(0,0,day++))
                        .setAmount((double)r.nextInt(500))
                        .setMemberId(ParseUser.getCurrentUser().getObjectId())
                        .setMemberName(ParseUser.getCurrentUser().getUsername())
                        .setCategory(categories.get(r.nextInt(size)% size));
                expenses.add(builder.build());
            }
            PersonalExpense.saveAll(PersonalExpenseActivity.this, testACL, expenses);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            loadingSpinner.setVisibility(View.INVISIBLE);
            demoButton.setClickable(true);
        }
    }
}
