package com.kmcheung.spendwise;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;
import org.achartengine.chart.PointStyle;

import java.security.PublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by alancheung on 30/11/14.
 */
public class Util {
    private static PointStyle[] styles = {
            PointStyle.POINT,
            PointStyle.DIAMOND,
            PointStyle.SQUARE,
            PointStyle.TRIANGLE,
            PointStyle.X,
            PointStyle.CIRCLE
    };
    private static int[] colors = {0xffea3f3f, 0xffea8c3f, 0xffead13f, 0xff39e13f, 0xff3feacb, 0xff3a8fea, 0xffbc3aea, 0xffea97ea, 0xffb3ea97, 0xff09a9ea};
//    private static int[] colors = {0xffea3f3f, 0xffea8c3f, 0xffead13f, 0xff39e13f, 0xff3feacb, 0xff3a8fea, 0xffbc3aea};
//    private static int[] colors = {Color.BLUE, Color.GREEN, Color.LTGRAY, Color.RED, Color.CYAN, Color.MAGENTA, Color.YELLOW};
    private static int styleCounter = 0;
    private static int colorCounter = 0;

    public static PointStyle nextPointStyle(){
        PointStyle style = styles[styleCounter% styles.length];
        styleCounter ++;
        return style;
    }

    public static int nextColor() {
        int color = colors[colorCounter % colors.length];
        colorCounter++;
        return color;
    }

    public static Date getDate(int year, int mon, int day) {
        mon --; //month is 0- based
        Calendar cal = Calendar.getInstance();
        cal.setLenient(false);
        cal.set(year, mon, day);
        return cal.getTime();
    }

    public static String getDateInDDMMYYYY(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String formattedDate = dateFormat.format(date);
        return formattedDate;
    }

    public static String getTimeStamp(Date basedate, Date currentDate, int binSizeInDay) {
        Calendar baseCal = Calendar.getInstance();
        Calendar currCal = Calendar.getInstance();
        baseCal.setTime(basedate);
        baseCal.set(Calendar.MILLISECOND, 0);
        currCal.setTime(currentDate);
        currCal.set(Calendar.MILLISECOND, 0);
        long dayDiff = (currCal.getTime().getTime() - baseCal.getTime().getTime())/(24*60*60*1000);
        int numOfBin = (int) (dayDiff/binSizeInDay);
        baseCal.add(Calendar.DAY_OF_MONTH, numOfBin * binSizeInDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        return dateFormat.format(baseCal.getTime());
    }

    public static Date getDateFromDDMMYYYY(String date) throws ParseException {
        if (date.length() != 8) throw new ParseException("Invalid format", 0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date myDate = dateFormat.parse(date);
        return myDate;
    }

    public static Date timeBeforeNow(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -year);
        cal.add(Calendar.MONTH, -month);
        cal.add(Calendar.DAY_OF_MONTH, -day);
        return cal.getTime();
    }

    public static boolean isSameDate(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        int y1 = cal1.get(Calendar.YEAR);
        int m1 = cal1.get(Calendar.MONTH);
        int d1 = cal1.get(Calendar.DAY_OF_MONTH);
        int y2 = cal2.get(Calendar.YEAR);
        int m2 = cal2.get(Calendar.MONTH);
        int d2 = cal2.get(Calendar.DAY_OF_MONTH);
        return (y1 == y2 && m1 == m2 && d1 == d2);
    }

    public static Date getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public static void makeToast(Context context, String msg) {
        Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
