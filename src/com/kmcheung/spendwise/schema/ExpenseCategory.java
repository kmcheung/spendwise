package com.kmcheung.spendwise.schema;

/**
 * Created by alancheung on 23/11/14.
 */
public final class ExpenseCategory {
    //Base type
    public static final String Transport = "Transport";
    public static final String Food = "Food";
    public static final String Entertainment = "Entertainment";
    public static final String Clothing = "Clothing";
    public static final String Groceries = "Groceries";
    public static final String Accomodation = "Accomodation";
    public static final String Travel = "Travel";
    public static final String Telecommunication = "Telecommunication";
    public static final String Others = "Others";

    //Extended type
//    public static final String Medical = "Medical";
//    public static final String Charity = "Charity";
//    public static final String Utilities = "Utilities";
//    public static final String Education = "Education";
//    public static final String Insurance = "Insurance";
//    public static final String Tax = "Tax";
}
