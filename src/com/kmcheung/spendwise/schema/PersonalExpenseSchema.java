package com.kmcheung.spendwise.schema;

/**
 * Created by alancheung on 23/11/14.
 */
public final class PersonalExpenseSchema {
    public static final String memberName = "MemberName";
    public static final String memberId = "MemberId";
    public static final String className = "PersonalExpense";
    public static final String category = "Category";
    public static final String date = "DateOfSpending";
    public static final String amount = "Amount";
    public static final String description = "Description";
}
