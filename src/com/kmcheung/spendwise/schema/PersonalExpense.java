package com.kmcheung.spendwise.schema;

import android.content.Context;
import com.kmcheung.spendwise.Util;
import com.parse.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alancheung on 6/12/14.
 */
public class PersonalExpense {

    private String memberName;
    private String memberId;
    private String className;
    private String category;
    private Date date;
    private Double amount;
    private String description;

    private PersonalExpense(Builder builder) {
        this.className = this.getClass().getSimpleName();
        this.memberName = builder.memberName;
        this.memberId = builder.memberId;
        this.category = builder.category;
        this.date = builder.date;
        this.amount = builder.amount;
        this.description = builder.description;
    }

    private static ParseObject toParse(PersonalExpense pe) {
        ParseObject expense = ParseObject.create(pe.className);
        if (pe.memberId != null && !pe.memberId.isEmpty()) {
            expense.put(PersonalExpenseSchema.memberId, pe.memberId);
        } else {
            expense.put(PersonalExpenseSchema.memberId, ParseUser.getCurrentUser().getObjectId());
        }

        if (pe.memberName != null && !pe.memberName.isEmpty()) {
            expense.put(PersonalExpenseSchema.memberName, pe.memberName);
        } else {
            expense.put(PersonalExpenseSchema.memberName, ParseUser.getCurrentUser().getUsername());
        }

        expense.put(PersonalExpenseSchema.description, pe.description);
        expense.put(PersonalExpenseSchema.amount, pe.amount);
        expense.put(PersonalExpenseSchema.category, pe.category);
        expense.put(PersonalExpenseSchema.date, pe.date);

        return expense;
    }

    public static void save(ParseACL acl, PersonalExpense pe) {
        ParseObject po = toParse(pe);
        po.setACL(acl);
        po.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void save(final Context context, PersonalExpense pe) {
        ParseObject po = toParse(pe);
        po.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Util.makeToast(context, e.getMessage());
                } else {
                    Util.makeToast(context, "Expense saved!");
                }
            }
        });
    }

    public static void saveAll(final Context context, ParseACL acl, List<PersonalExpense> expenses) {
        List<ParseObject> parseObjects = new ArrayList<ParseObject>();
        for (PersonalExpense expense: expenses) {
            ParseObject po = toParse(expense);
            po.setACL(acl);
            parseObjects.add(po);
        }
        ParseObject.saveAllInBackground(parseObjects, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Util.makeToast(context, e.getMessage());
                    e.printStackTrace();
                } else {
                    Util.makeToast(context, "Expenses saved!");
                }
            }
        });
    }

    public static void saveAll(List<PersonalExpense> expenses) {
        List<ParseObject> parseObjects = new ArrayList<ParseObject>();
        for (PersonalExpense expense: expenses) {
           parseObjects.add(toParse(expense));
        }
        ParseObject.saveAllInBackground(parseObjects, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static class Builder {
        private String memberName;
        private String memberId;
        private String category;
        private Date date;
        private Double amount;
        private String description;

        public Builder setMemberName(String memberName) {
            this.memberName = memberName;
            return this;
        }

        public Builder setMemberId(String memberId) {
            this.memberId = memberId;
            return this;
        }

        public Builder setCategory(String category) {
            this.category = category;
            return this;
        }

        public Builder setDate(Date date) {
            this.date = date;
            return this;
        }

        public Builder setAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public PersonalExpense build() {
            return new PersonalExpense(this);
        }
    }

}
