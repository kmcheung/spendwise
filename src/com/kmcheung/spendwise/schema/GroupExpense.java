package com.kmcheung.spendwise.schema;

/**
 * Created by alancheung on 23/11/14.
 */
public final class GroupExpense {
    public static final String groupName = "GroupName";
    public static final String groupId = "GroupId";
    public static final String className = "GroupExpense";
    public static final String members = "Members";
    public static final String category = "Category";
    public static final String date = "Date";
    public static final String amount = "Amount";
    public static final String description = "Description";
}
