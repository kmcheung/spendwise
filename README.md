### Compilation Steps

Eclipse

1. File → Import → General → Existing Projects into Workspace → Next
2. Select root directory: `/some/path/to/SpendWise`
3. Projects → Select All
4. Uncheck Copy projects into workspace and Add project to working sets
5. Finish

Intellij

1. File → Import Project
2. Type `/some/path/to/SpendWise` in path → OK
3. Create project from existing source
4. Use default settings and press Next all the way down
5. Finish

The project should contain  
1. src - all the source code  
2. libs - dependencies of project  
3. res - resources for the project

Project can be found at
[https://bitbucket.org/kmcheung/spendwise](https://bitbucket.org/kmcheung/spendwise)